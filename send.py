#!/usr/bin/env python

import os

from flask import Flask, render_template
from flask_mail import Mail, Message
from config import EMAIL_USER, EMAIL_PASSWORD

app = Flask(__name__)

mail_settings = {
    "MAIL_SERVER": 'smtp.gmail.com',
    "MAIL_PORT": 465,
    "MAIL_USE_TLS": False,
    "MAIL_USE_SSL": True,
    "MAIL_USERNAME": EMAIL_USER,
    "MAIL_PASSWORD": EMAIL_PASSWORD,
}

app.config.update(mail_settings)
mail = Mail(app)

emails_to = ["",
             "",
             ]

body_text = "This is a test email I sent with Gmail and Python! Here is a link to a script file: https://gitlab.com/VOlni/send_email_flask_app/-/tree/master/"

if __name__ == '__main__':
    with app.app_context():
        msg = Message(subject="Test mail",
                      sender=app.config.get("MAIL_USERNAME"),
                      recipients=emails_to, # replace with your email for testing
                      body=body_text)
        msg.html = render_template("email.html")
        working_dir=os.getcwd()
        with app.open_resource("docs/attachment_doc.docx", 'rb') as fh:
            msg.attach("attachment_doc.docx", "docs/docx", fh.read())
        mail.send(msg)
